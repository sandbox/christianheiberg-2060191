These two modules, ch_debugger and ch_nodeapi_preexec, are hook debuggers, 
used for collecting arrays and objects and store the data in a table for later
analysis.

As the module is now, it's made for listening on nodeapi, user, form_alter,
views_pre_view, views_pre_build and views_pre_execute.
If you want to create other/different hooks, please follow the logic's of
the existing six hooks in this module.

You can by advance install 'Xdebug' from xdebug.org.
If that PHP plugin is enabled, the debugger will store the call line in the
table, which can be pretty help full, when debugging.

Getting started:

Install both modules, and set the permission for 'administer site configuration'
Now enter /admin/settings/ch_debugger and set your needs and role permissions.
By role, I mean a role that may trig a hook.

All the output goes to /admin/reports/ch_debugger, where you can see all the
collected data, grouped in php instances.

In the case of following a nodeapi, you can enable the 'ch_nodeapi_preexec'
module and get a hook running time on every nodeapi hooks.
This module places a time stamp inside the nodeapi object, to follow.
This can be very help full when working with performance issues.

Just like the Devel module's dpm / dvm function, this module got a sdd($obj)
function that stores data when needed
The syntax of sdd is as shown.
    
    $object         = $user;
    $name           = "what is happening at line 43?";
    $special_data[] = $object->uid;
    $special_data[] = $object->roles;
    
    sdd ( $object , $name, $special_data);

The idea of using the special data field is a better explanative listing.
Note: special_data will only displays data from the 1st array level.
      To much data in that field, and the overview is lost !!!!

Good luck
